describe("Pay Button Appearance", () => {
	test("Users can't be paid if they do not have an application", () => {
		expect(findUser(applications, userWithApplication)).toEqual(true);
		expect(findUser(applications, userWithOutApplication)).toEqual(false);
		expect(findUser(applications, userWithApplicationNotPaid)).toEqual(false);
	});

	// TODO: actual test should be for appearance of pay button
});

const applications = [
	{
		uuid: "01b35179-134c-4bb1-af36-a9663c009fcd",
		userUuid: "4aaf4534-531d-4c98-b838-23460fb34e5b",
		requestedAmount: 44798
	},
	{
		uuid: "023be91c-2f95-41f2-9c3c-2e871450b653",
		userUuid: "265d6cf3-4a79-4cc7-815f-2934e0206765",
		requestedAmount: 40058
	},
	{
		uuid: "028248b4-eb6e-4e75-8a0d-7f4eba7488c6",
		userUuid: "473cd1b9-eca0-482f-9e5a-2d1aec8a4e4a",
		requestedAmount: 25551
	},
	{
		uuid: "02ba4b80-88ff-40f1-9c74-34f52c931ba3",
		userUuid: "22c8fcee-e09d-47e1-bd25-07bbc054d31f",
		requestedAmount: 25233
	},
	{
		uuid: "05993010-da7c-45da-9654-239c403921dc",
		userUuid: "0918a5f3-4ddd-41c0-beaf-79e67ce7cdb8",
		requestedAmount: 42182
	},
	{
		uuid: "baec2656-27c9-4495-887f-7d690cf56f59",
		userUuid: "5cce3343-4360-44e7-8014-b226ee0e38ea",
		requestedAmount: 38169
	}
]

const payments = [
	{
		uuid: "79b12375-839c-4418-8304-e48b7dacdf1f",
		applicationUuid: "02ba4b80-88ff-40f1-9c74-34f52c931ba3",
		paymentMethod: "ACH",
		paymentAmount: 25233
	},
	{
		uuid: "e4068685-69bf-44c3-97e9-7364ed1b51a7",
		applicationUuid: "05993010-da7c-45da-9654-239c403921dc",
		paymentMethod: "ACH",
		paymentAmount: 42182
	},
	{
		uuid: "fce40e75-72f8-47f3-9a1c-ff745da9dfb5",
		applicationUuid: "05b5b034-b001-459a-94b6-a899f9ab08a5",
		paymentMethod: "ACH",
		paymentAmount: 40110
	},
	{
		uuid: "ae2b020d-d8b7-409d-988a-38c136945986",
		applicationUuid: "05b86e6a-f751-48ff-9a34-2240c96fa7a8",
		paymentMethod: "ACH",
		paymentAmount: 47717
	},
	{
		uuid: "61494ceb-cad6-41a8-b4a2-4a44d2740e07",
		applicationUuid: "06c4f45c-781e-473f-88eb-99726e6bf60b",
		paymentMethod: "ACH",
		paymentAmount: 46469
	}
]

const userWithApplication = {
	uuid: "0918a5f3-4ddd-41c0-beaf-79e67ce7cdb8",
	name: "Sigismundo Forsdicke",
	email: "sforsdicke7m@wufoo.com"
}

const userWithOutApplication = {
	uuid: "8df54a3b-ee1b-493d-b1dc-ba3ec144e529",
	name: "Georgetta Helwig",
	email: "ghelwig74@linkedin.com"
}

const userWithApplicationNotPaid = {
	uuid: "5cce3343-4360-44e7-8014-b226ee0e38ea",
	name: "Quinton Bogey",
	email: "qbogey4o@cafepress.com"
}

function findUser(applications, user){
	const application = applications.find(app => app.userUuid === user.uuid)
	if(application){
		const payment = payments.find(pmt => application.uuid === pmt.applicationUuid)
		if(payment){
			return true
		}
	} 
	return false
}